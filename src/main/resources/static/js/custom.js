//$(document).ready(function () {
//    $(".btn-primary").click(function () {
//        $(".collapse").collapse('toggle');
//    });
//    $(".btn-success").click(function () {
//        $(".collapse").collapse('show');
//    });
//    $(".btn-warning").click(function () {
//        $(".collapse").collapse('hide');
//    });
//    $(".collapse").on('show.bs.collapse', function () {
//        alert('The collapsible content is about to be shown.');
//    });
//    $(".collapse").on('shown.bs.collapse', function () {
//        alert('The collapsible content is now fully shown.');
//    });
//    $(".collapse").on('hide.bs.collapse', function () {
//        alert('The collapsible content is about to be hidden.');
//    });
//    $(".collapse").on('hidden.bs.collapse', function () {
//        alert('The collapsible content is now hidden.');
//    });
//});

function openModalFormMenu() {

    $('#modalBodyFormMenu').load('tes/menu/add', function () {
        $('#modalFormMenu').modal({show: true});
        //reload();
    });

    // $("#demo").load(location.href + " #demo");

}


//===========

function openDaftarRincianCollapse(id, id2, id3) {
    $.ajax({
        url: "/ep/tugaskegiatan/detil/" + id,
        success: function (data) {
//            $("#group-of-rows-" + id + "-" + id2 + "-" + id3).collapse('toggle');
            $("#collapseHolderRincian-" + id + "-" + id2 + "-" + id3).html(data);
            $("#group-of-rows-" + id + "-" + id2 + "-" + id3).collapse('toggle');
        }
    });

}

function openDataLogModal(id) {

    $.ajax({
        url: "/ep/tugaskegiatan/" + id,
        success: function (data) {
            //console.log(data);
            $("#modalHolderLog").html(data);
            $("#modalLog").modal("show");
        }
    });

}

function deleteDataKegiatan(id) {

    $.ajax({
        url: "delete?id=" + id,
        success: function (data) {
            //console.log(data);
            var newURL = location.href.split("&")[0];
            window.history.pushState('object', document.title, newURL);

            reload();
            $("#modalInfoHolder").html(data);
            if (data.info === 'sukses') {
                $("#modalInfoSukses").modal("show");
            } else
            {
                $("#modalInfoGagal").modal("show");
            }

        }
    });

}

function deleteDataDetilKegiatan(id) {

    $.ajax({
        url: "detil/delete?id=" + id,
        success: function (data) {
            //console.log(data);
            //window.location.search = null;
            var newURL = location.href.split("&")[0];
            window.history.pushState('object', document.title, newURL);

            reload();
            $("#modalHolderInfo").html(data);
            if (data.info === 'sukses') {
                $("#modalInfoSukses").modal("show");
            } else
            {
                $("#modalInfoGagal").modal("show");
            }

        }
    });

}

function deleteDataPetugas(id) {

    $.ajax({
        url: "delete/petugas?id=" + id,
        success: function (data) {
            //console.log(data);
            //window.location.search = null;
            var newURL = location.href.split("&")[0];
            window.history.pushState('object', document.title, newURL);

            reload();
            $("#modalHolderInfo").html(data);
            if (data.info === 'sukses') {
                $("#modalInfoSukses").modal("show");
            } else
            {
                $("#modalInfoGagal").modal("show");
            }

        }
    });

}

function openModalFormKegiatan(id) {

    $('#modalBodyFormKegiatan').load('form?id2=' + id, function () {
        $('#modalFormKegiatan').modal({show: true});
        //reload();
    });

    // $("#demo").load(location.href + " #demo");

}

function openModalFormPetugas(id) {

    $('#modalBodyFormPetugas').load('petugas/form?id=' + id, function () {
        $('#modalFormPetugas').modal({show: true});
        //reload();
    });

    // $("#demo").load(location.href + " #demo");

}

function openModalFormDetilKegiatan(id) {

    $('#modalBodyFormDetilKegiatan').load('detil/form?id=' + id, function () {
        $('#modalFormDetilKegiatan').modal({show: true});
    });


}

function openModalFormEditDetilKegiatan(id, id2) {

    $('#modalBodyFormDetilKegiatan').load('detil/form?id=' + id + '&id2=' + id2, function () {
        $('#modalFormDetilKegiatan').modal({show: true});
    });

}

function simpanDataDetilKegiatan(e) {
    e.preventDefault();
    var data = $('#form-detil-kegiatan').serialize();
    $.ajax({
        url: "ep/tugaskegiatan/detil/form",
        type: 'GET',
        data: data,
        success: function (data) {
            //console.log(data);
            reload();
            $("#modalHolderInfo").html(data);
            if (data.info === 'sukses') {
                $("#modalInfoSukses").modal("show");
            } else
            {
                $("#modalInfoGagal").modal("show");
            }
        }
    });

}

function updateDataDetilKegiatan(id) {
    $.ajax({
        url: "detil/update?id=" + id,
        success: function (data) {
            //console.log(data);
            //window.location.search = null;
            var newURL = location.href.split("&")[0];
            window.history.pushState('object', document.title, newURL);

            reload();
            $("#modalHolderInfo").html(data);
            if (data.info === 'sukses') {
                $("#modalInfoSukses").modal("show");
            } else
            {
                $("#modalInfoGagal").modal("show");
            }

        }
    });

}

function openDataRincianPublikModal(id) {

    $.ajax({
        url: "/beranda/hasil/" + id,
        success: function (data) {
            //console.log(data);
            $("#modalHolderRincianPublik").html(data);
            $("#modalRincianPublik").modal("show");
        }
    });

}

function hideModal() {
    $(".modal").removeClass("in");
    $(".modal-backdrop").remove();
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '');
    $(".modal").hide();
}
function refreshPage() {
    window.location.reload();
}

function reload() {
    {
        $("#kolomKegiatan").load(window.location.href + " #kolomKegiatan");
        $("#kolomPetugas").load(window.location.href + " #kolomPetugas");
        $("#kolomProgres").load(window.location.href + " #kolomProgres");
        hideModal();
    }

}

function getUrlVars() {
    var vars = {};
    var parts = window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,
            function (m, key, value) {
                vars[key] = decodeURIComponent(value);
            });

    return vars;
}


$(document).ready(function () {

    var fType = getUrlVars()["status"];

    if (fType === "sukses") {
        $("#modalInfoSukses").modal("show");
    }

});