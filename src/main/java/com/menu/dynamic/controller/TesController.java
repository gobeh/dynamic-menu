/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.menu.dynamic.controller;

import com.menu.dynamic.dao.MenuDao;
import com.menu.dynamic.dao.OrganizationMenuDao;
import com.menu.dynamic.dao.OrganizationRepository;
import com.menu.dynamic.model.Menu;
import com.menu.dynamic.model.Organization;
import com.menu.dynamic.model.OrganizationMenu;
import com.menu.dynamic.model.User;
import com.menu.dynamic.services.CurrentUserService;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Ahmad
 */
@Controller
public class TesController {

    @Autowired
    private MenuDao menuDao;

    @Autowired
    private OrganizationRepository orgRepo;

    @Autowired
    private OrganizationMenuDao orgMenuDao;

    @Autowired
    private CurrentUserService currentUserService;

    @RequestMapping(value = "/tes", method = RequestMethod.GET)
    public String beranda(Model model, Authentication auth) {

        List<Menu> daftarMenu = (List<Menu>) menuDao.findAll();

        List<Menu> menuList = menuDao.findByParentIsNullOrderByIdAsc();

        User u = currentUserService.currentUser(auth);

        List<OrganizationMenu> menuListOrg = orgMenuDao.findByOrganizationAndMenuParentIsNull(u.getOrganization());

        List<Menu> menuU = new LinkedList<>();

        //menuU.addAll(menuList);
        long idcek;
        long idcek2;
        int i = 0;
        int j = 0;

        for (OrganizationMenu om : menuListOrg) {

            menuU.add(menuListOrg.get(i).getMenu());
//            idcek = menuListOrg.get(i).getMenu().getId();
//            for (Menu m : menuU) {
//                if (idcek != menuU.get(j).getId()) {
//                    menuU.remove(j);
//                    j = 0;
//                    break;
//                }
//                j++;
//            }
            i++;
        }

//        i = 0;
//        j = 0;
//        for (OrganizationMenu om : menuListOrg) {
//
//            // menuU.add(menuListOrg.get(i).getMenu());
//            idcek = menuListOrg.get(i).getMenu().getChildren().size();
//            if (idcek != 0) {
//                while (j < idcek) {
//                    idcek2 = menuListOrg.get(i).getMenu().getChildren().get(j).getId();
//                    if (menuU.get(i).getChildren().get(j).getId() == idcek2) {
//
//                    }
//                }
//            }
////            for (Menu m : menuU) {
////                if (idcek != menuU.get(j).getId()) {
////                    menuU.remove(j);
////                    j = 0;
////                    break;
////                }
////                j++;
////            }
//            i++;
//        }

//        for (Menu m : daftarMenu) {
//            recursiveTree(m);
//        }
        model.addAttribute("daftarmenu", daftarMenu);
        model.addAttribute("menulist", menuList);
        model.addAttribute("menulistorg", menuListOrg);
        model.addAttribute("menuu", menuU);

        return "tes";
    }

    @GetMapping(value = "/tes/menu/add")
    public String addMenu(Model model) {
        List<Menu> daftarMenu = (List<Menu>) menuDao.findAll();
        List<Organization> daftarOrganisasi = orgRepo.findAll();

        model.addAttribute("daftarmenu", daftarMenu);
        model.addAttribute("daftarorganisasi", daftarOrganisasi);

        return "form";
    }

    @PostMapping(value = "/tes/menu/add")

    public String saveMenu(@RequestParam(name = "nama") String nama,
            @RequestParam(name = "url") String url,
            @RequestParam(name = "parent") Menu parent,
            @RequestParam(name = "organisasi") Organization organisasi, Model model) {

        Menu menuBaru = new Menu();
        menuBaru.setNama(nama);
        menuBaru.setUrl(url);
        menuBaru.setParent(parent);

        menuDao.save(menuBaru);

        OrganizationMenu orgBaru = new OrganizationMenu();
        orgBaru.setMenu(menuBaru);
        orgBaru.setOrganization(organisasi);

        orgMenuDao.save(orgBaru);

        return "redirect:/tes";
    }

//    public void recursiveTree(Menu m) {
//        System.out.println(m.getNama());
//        if (m.getChildren().size() > 0) {
//            for (Menu c : m.getChildren()) {
//                recursiveTree(c);
//            }
//        }
//    }
}
