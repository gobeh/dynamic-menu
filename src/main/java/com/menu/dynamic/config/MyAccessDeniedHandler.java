package com.menu.dynamic.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 *
 * @author Bang Pardi
 */
@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {

    private static Logger LOG = LoggerFactory.getLogger(MyAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ade) throws IOException, ServletException {
        Authentication auth
                = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            LOG.info(String.format("User '%s' Authority '%s' attempted to access the protected URL: %s", auth.getName(), auth.getAuthorities(), request.getRequestURI()));
        }

        response.sendRedirect(request.getContextPath() + "/403");
    }

}
