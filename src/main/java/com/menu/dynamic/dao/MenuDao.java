/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.menu.dynamic.dao;

import com.menu.dynamic.model.Menu;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Ahmad
 */
public interface MenuDao extends PagingAndSortingRepository<Menu, Long>{
    
    List<Menu> findByParentIsNullOrderByIdAsc();
    
    List<Menu> findByParentIsNull();
    
    List<Menu> findAllByOrderByIdAsc();
}
