/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.menu.dynamic.dao;

import com.menu.dynamic.model.Organization;
import com.menu.dynamic.model.OrganizationMenu;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Ahmad
 */
public interface OrganizationMenuDao extends PagingAndSortingRepository<OrganizationMenu, Long> {

    List<OrganizationMenu> findByOrganizationAndMenuParentNotNull(Organization org);
    
    List<OrganizationMenu> findByOrganizationAndMenuParentIsNull(Organization org);
    
    List<OrganizationMenu> findByOrganization(Organization org);
    
    List<OrganizationMenu> findByOrganizationOrderByMenuIdAsc(Organization org);
}
