package com.menu.dynamic.services;


import com.menu.dynamic.dao.UserRepository;
import com.menu.dynamic.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 *
 * @author Bang Pardi
 */
@Service
public class CurrentUserService {

    private static final Logger log = LoggerFactory.getLogger(CurrentUserService.class);

    @Autowired
    private UserRepository userDao;

    public User currentUser(Authentication auth) {
        String username = auth.getName();
        User u = userDao.findByUsername(username);
        //log.info("Info User -> username {} role {}", u.getUsername(), u.getRole().getName());

        return u;
    }
}
